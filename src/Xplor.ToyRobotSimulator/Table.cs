﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xplor.ToyRobotSimulator
{
    public class Table
    {
        private readonly int _rows;
        private readonly int _columns;

        public Table(int columns, int rows)
        {
            _columns = columns;
            _rows = rows;
        }

        public bool IsValidPosition(Position position)
        {
            if (position == null)
                return false;

            return position.X >= 0 && position.X < _columns &&
                   position.Y >= 0 && position.Y < _rows;
        }
    }
}
