﻿using System;
using System.Collections.Generic;
using System.Text;
using Xplor.ToyRobotSimulator.Output;

namespace Xplor.ToyRobotSimulator
{
    public class Robot
    {
        private IOutputWriter _outputWriter;

        private Position _position;
        private Direction _direction;
        private Table _table;

        public Robot(IOutputWriter outputWriter)
        {
            _outputWriter = outputWriter;
        }

        public void Place(Table table, Position position, Direction direction)
        {
            if (table == null ||
                position == null ||
                !table.IsValidPosition(position))
                return;

            _table = table;
            _position = position;
            _direction = direction;
        }

        public void Move(int steps)
        {
            if (!IsPlaced)
                return;

            var nextPosition = CalculateNextPosition(steps);
            if (!_table.IsValidPosition(nextPosition))
                return;

            _position = nextPosition;
        }

        public void Report()
        {
            if (!IsPlaced)
                return;

            _outputWriter.WriteLine($"{_position.X},{_position.Y},{_direction}");
        }

        public void RotateLeft()
        {
            if (!IsPlaced)
                return;

            _direction = (Direction)(((int)_direction + 3) % 4);
        }

        public void RotateRight()
        {
            if (!IsPlaced)
                return;

            _direction = (Direction)(((int)_direction + 1) % 4);
        }

        private bool IsPlaced => _table != null && _position != null;

        // This method can be moved to another class, e.g. PositionCalculator so that it can be unit tested  
        // but for this example, i think it's very simple so i'll just leave it here for now
        private Position CalculateNextPosition(int steps)
        {
            switch (_direction)
            {
                case Direction.NORTH:
                    return new Position(_position.X, _position.Y + steps);
                case Direction.EAST:
                    return new Position(_position.X + steps, _position.Y);
                case Direction.SOUTH:
                    return new Position(_position.X, _position.Y - steps);
                case Direction.WEST:
                    return new Position(_position.X - steps, _position.Y);
                default:
                    throw new ArgumentOutOfRangeException(nameof(_direction), "Invalid direction");
            }
        }
    }
}
