﻿namespace Xplor.ToyRobotSimulator.Commands
{
    public interface ICommand
    {
        void Execute();
    }
}
