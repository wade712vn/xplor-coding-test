﻿namespace Xplor.ToyRobotSimulator
{
    public class Position
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Position(int x, int y)
        {
            X = x;
            Y = y;
        }

        public override bool Equals(object? obj)
        {
            if ((obj == null) || this.GetType() != obj.GetType())
            {
                return false;
            }
            else
            {
                Position p = (Position) obj;
                return X == p.X && Y == p.Y;
            }
        }
    }
}