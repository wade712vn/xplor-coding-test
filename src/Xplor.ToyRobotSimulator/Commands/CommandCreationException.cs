﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xplor.ToyRobotSimulator.Commands
{
    public class CommandCreationException : Exception
    {
        private string _commandStr;

        public string CommandStr => _commandStr;

        public CommandCreationException(string commandStr, string message) : base(message)
        {
        }
    }
}
