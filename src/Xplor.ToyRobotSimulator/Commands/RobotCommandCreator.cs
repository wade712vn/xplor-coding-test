﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Xplor.ToyRobotSimulator.Commands
{
    public class RobotCommandCreator : ICommandCreator
    {
        public ICommand CreateCommand(string commandStr, Robot robot, Table table)
        {
            if (string.IsNullOrWhiteSpace(commandStr))
                throw new CommandCreationException(commandStr, "Invalid command");

            var parts = commandStr.Split(new[] { ' ', ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (!parts.Any())
                throw new CommandCreationException(commandStr, "Invalid command");

            var command = parts[0].Trim();

            switch (command)
            {
                case "PLACE":
                    if (parts.Length != 4 ||
                        !int.TryParse(parts[1], out var x) ||
                        !int.TryParse(parts[2], out var y) ||
                        !Enum.TryParse(parts[3], false, out Direction direction))
                    {
                        throw new CommandCreationException(commandStr, "Invalid params for PLACE command");
                    }
                    return new PlaceCommand(robot, table, new Position(x, y), direction);
                case "MOVE":
                    return new MoveCommand(robot, 1); // Move robot 1 step by default
                case "LEFT":
                    return new LeftCommand(robot);
                case "RIGHT":
                    return new RightCommand(robot);
                case "REPORT":
                    return new ReportCommand(robot);
                default:
                    throw new CommandCreationException(commandStr, "Invalid command");
            }
        }
    }
}
