﻿namespace Xplor.ToyRobotSimulator.Commands
{
    public abstract class RobotCommand : ICommand
    {
        protected Robot _robot;

        public Robot Robot => _robot;

        protected RobotCommand(Robot robot)
        {
            _robot = robot;
        }

        public abstract void Execute();
    }
}
