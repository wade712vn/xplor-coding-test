﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;

namespace Xplor.ToyRobotSimulator.Tests
{
    public class TableTests
    {

        [TestCase(5, 5, 1, 2, true)]
        [TestCase(5, 5, 5, 4, false)]
        [TestCase(5, 5, 4, 4, true)]
        [TestCase(5, 2, 4, 4, false)]
        public void Test_IsValidPosition(int columns, int rows, int x, int y, bool expectedIsValid)
        {
            var table = new Table(columns, rows);
            var isValid = table.IsValidPosition(new Position(x, y));
            Assert.AreEqual(expectedIsValid, isValid);
        }
    }
}
