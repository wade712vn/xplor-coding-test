﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xplor.ToyRobotSimulator
{
    public enum Direction
    {
        NORTH,
        EAST,
        SOUTH,
        WEST
    }
}
