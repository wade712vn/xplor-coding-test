﻿using System;
using System.Linq;
using Xplor.ToyRobotSimulator.Commands;

namespace Xplor.ToyRobotSimulator
{
    public class Simulator
    {
        private Table _table;
        private Robot _robot;

        private ICommandCreator _commandCreator;

        public Simulator(Table table, Robot robot, ICommandCreator commandCreator)
        {
            _table = table;
            _robot = robot;

            _commandCreator = commandCreator;
        }

        public void ProcessCommands(string[] commands)
        {
            if (commands == null || !commands.Any())
                return;

            foreach (var commandStr in commands)
            {
                try
                {
                    var command = _commandCreator.CreateCommand(commandStr, _robot, _table);
                    command.Execute();
                }
                catch (CommandCreationException)
                {
                    // Ignore if command is invalid
                }
            }
        }
    }
}
