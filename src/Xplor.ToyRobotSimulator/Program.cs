﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Xplor.ToyRobotSimulator.Commands;
using Xplor.ToyRobotSimulator.Output;

namespace Xplor.ToyRobotSimulator
{
    class Program
    {
        static void Main(string[] args)
        {
            var table = new Table(5, 5);

            var consoleOutputWriter = new ConsoleOutputWriter();
            var robot = new Robot(consoleOutputWriter);

            var commands = new[] { "PLACE 4,4,EAST", "MOVE", "MOVE", "MOVE", "MOVE", "REPORT" };

            var robotCommandCreator = new RobotCommandCreator();
            var simulator = new Simulator(table, robot, robotCommandCreator);
            simulator.ProcessCommands(commands);
        }
    }
}
