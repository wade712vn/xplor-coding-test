﻿using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using NUnit.Framework;
using Xplor.ToyRobotSimulator.Commands;
using Xplor.ToyRobotSimulator.Output;

namespace Xplor.ToyRobotSimulator.Tests
{
    public class RobotCommandCreatorTests
    {
        private static Table _table = new Table(5, 5);
        private static Robot _robot = new Robot(new Mock<IOutputWriter>().Object);

        static IEnumerable<TestCaseData> ValidCommands
        {
            get
            {
                yield return new TestCaseData("MOVE", new MoveCommand(_robot, 1));
                yield return new TestCaseData("MOVE 12", new MoveCommand(_robot, 1));
                yield return new TestCaseData("PLACE 1,3,NORTH", new PlaceCommand(_robot, _table, new Position(1,3), Direction.NORTH));
                yield return new TestCaseData("LEFT", new LeftCommand(_robot));
                yield return new TestCaseData("RIGHT", new RightCommand(_robot));
                yield return new TestCaseData("REPORT", new ReportCommand(_robot));
            }
        }

        static IEnumerable<TestCaseData> InvalidCommands
        {
            get
            {
                yield return new TestCaseData("ABC", new CommandCreationException("", "Invalid exception"));
                yield return new TestCaseData("ABC", new CommandCreationException("ABC", "Invalid exception"));
                yield return new TestCaseData("ABC", new CommandCreationException("PLACE 1", "Invalid params for PLACE command"));
            }
        }

        [TestCaseSource("ValidCommands")]
        public void Test_CreateCommand_ValidCommandStr_ReturnCommand(string commandStr, ICommand expectedCommand)
        {
            var creator = new RobotCommandCreator();
            var command = creator.CreateCommand(commandStr, _robot, _table);

            Assert.AreEqual(expectedCommand.GetType(), command.GetType());
            if (expectedCommand.GetType() == command.GetType() && command is PlaceCommand placeCommand)
            {
                var expectedPlaceCommand = (PlaceCommand) expectedCommand;
                Assert.AreEqual(expectedPlaceCommand.Robot, placeCommand.Robot);
                Assert.AreEqual(expectedPlaceCommand.Table, placeCommand.Table);
                Assert.AreEqual(expectedPlaceCommand.Position, placeCommand.Position);
            }
        }

        [TestCaseSource("InvalidCommands")]
        public void Test_CreateCommand_InvalidCommandStr_ThrowException(string commandStr, CommandCreationException expectedException)
        {
            var creator = new RobotCommandCreator();
            var exception = Assert.Throws<CommandCreationException>(() => creator.CreateCommand(commandStr, _robot, _table));
            Assert.AreEqual(expectedException.CommandStr, expectedException.CommandStr);
            Assert.AreEqual(expectedException.Message, expectedException.Message);
        }
    }
}
