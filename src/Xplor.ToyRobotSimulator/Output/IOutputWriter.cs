﻿namespace Xplor.ToyRobotSimulator.Output
{
    public interface IOutputWriter
    {
        void WriteLine(string value);
    }
}