using System.Collections;
using Moq;
using NUnit.Framework;
using Xplor.ToyRobotSimulator.Commands;
using Xplor.ToyRobotSimulator.Output;

namespace Xplor.ToyRobotSimulator.Tests
{
    public class SimulatorTests
    {
        [TestCase("0,1,NORTH", "PLACE 0,0,NORTH", "MOVE", "REPORT")]
        [TestCase("0,0,WEST", "PLACE 0,0,NORTH", "LEFT", "REPORT")]
        [TestCase("4,4,NORTH", "PLACE 4,4,NORTH", "MOVE", "MOVE", "MOVE", "MOVE", "REPORT")]
        [TestCase("3,3,NORTH", "PLACE 1,2,EAST", "MOVE", "MOVE", "LEFT", "MOVE", "REPORT")]
        [TestCase("3,4,NORTH", "MOVE", "REPORT", "RIGHT", "PLACE 3,2,NORTH", "MOVE", "LEFT", "RIGHT", "MOVE", "REPORT")]
        public void Test_RobotPlaced_Report(string expectedOutput, params string[] commands)
        {

            var table = new Table(5, 5);
            var mockOutputWriter = new Mock<IOutputWriter>();
            var robot = new Robot(mockOutputWriter.Object);

            var robotCommandCreator = new RobotCommandCreator();
            var simulator = new Simulator(table, robot, robotCommandCreator);
            simulator.ProcessCommands(commands);

            mockOutputWriter.Verify(m => m.WriteLine(expectedOutput), Times.Exactly(1));
        }

        [TestCase("PLACE 10,0,NORTH", "MOVE", "REPORT")]
        [TestCase("LEFT", "REPORT")]
        [TestCase("MOVE", "MOVE", "LEFT", "PLACE 1,1 ABC", "MOVE", "REPORT")]
        public void Test_RobotNotPlaced_NoReport(params string[] commands)
        {

            var table = new Table(5, 5);
            var mockOutputWriter = new Mock<IOutputWriter>();
            var robot = new Robot(mockOutputWriter.Object);

            var robotCommandCreator = new RobotCommandCreator();
            var simulator = new Simulator(table, robot, robotCommandCreator);
            simulator.ProcessCommands(commands);

            mockOutputWriter.Verify(m => m.WriteLine(It.IsAny<string>()), Times.Never);
        }
    }
}