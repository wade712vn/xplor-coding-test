﻿namespace Xplor.ToyRobotSimulator.Commands
{
    public interface ICommandCreator
    {
        ICommand CreateCommand(string commandStr, Robot robot, Table table);
    }
}