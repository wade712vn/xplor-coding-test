# README #

This is Giang's implementation for the Toy Robot Simulator coding challenge

### Assumptions ###

* The program ignores and doesn't throw any error on any invalid command
* A command such as MOVE 12 will still be considered valid, the 12 part will be ignored

### Considerations ###

* The simulator issues PLACE, MOVE, REPORT commands to the robot no matter if they are valid actions to be performed, the robot makes its decision where it should perform the action 
* The method CalculateNextPosition in class Robot can be moved to another class so that it can be unit tested but for the simplicity of this problem, it can be left how it is
* Dependency injection - not necessary for such a simple program